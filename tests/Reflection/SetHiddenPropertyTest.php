<?php

namespace Cetria\Helpers\Reflection\Tests;

use function rand;
use function get_class;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use ReflectionException;

class setHiddenPropertyTest extends TestCase
{
    public function testMethod__withInstance__privateProperty(): void
    {
        $value = rand(1, 50);
        $instance = $this->getTestInstance();
        Reflection::setHiddenProperty($instance, 'privateProperty', $value);
        $this->assertEquals($instance->getPrivateProperty(), $value);
    }

    public function testMethod__withClass__privateStaticProperty(): void
    {
        $value = rand(1, 50);
        $instance = $this->getTestInstance();
        Reflection::setHiddenProperty(get_class($instance), 'privateStaticProperty', $value);
        $this->assertEquals($instance->getPrivateStaticProperty(), $value);
    }

    public function testMethod__nonexistentProperty(): void
    {
        $instance = $this->getTestInstance();
        $this->expectException(ReflectionException::class);
        Reflection::setHiddenProperty(\get_class($instance), 'nonexistentProperty', 2);
    }


    private function getTestInstance(): object
    {
        return new class() 
        {
            private static $privateStaticProperty;
            private $privateProperty;

            
            public function getPrivateStaticProperty()
            {
                return static::$privateStaticProperty;
            }

            public function getPrivateProperty()
            {
                return $this->privateProperty;
            }
        };
    }
}
