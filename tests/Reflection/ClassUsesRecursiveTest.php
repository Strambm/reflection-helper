<?php

namespace Cetria\Helpers\Reflection\Tests;

use function array_diff;
use function count;

use Cetria\Helpers\Reflection\ClassDoesNotExistException;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassUsesRecursiveTest extends TestCase
{
    public function testMethod__withInstance(): void
    {
        $instance = new Product(); 
        $traits = Reflection::classUsesRecursive($instance);
        $this->assertTrue(\in_array(HasFactory::class, $traits));
        $eloquentModelTraits = Reflection::classUsesRecursive(Model::class);
        $diffTraits = array_diff($eloquentModelTraits, $traits);
        $this->assertEquals(0, count($diffTraits));
    }

    public function testMethod__withClass(): void
    {
        $traits = Reflection::classUsesRecursive(Product::class);
        $this->assertTrue(\in_array(HasFactory::class, $traits));
        $eloquentModelTraits = Reflection::classUsesRecursive(Model::class);
        $diffTraits = array_diff($eloquentModelTraits, $traits);
        $this->assertEquals(0, count($diffTraits));
    }

    public function testMethod__nonexistModelName(): void
    {
        $class = 'NonexistClass';
        $this->expectException(ClassDoesNotExistException::class);
        Reflection::classUsesRecursive($class);
    }
}