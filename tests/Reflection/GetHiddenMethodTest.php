<?php

namespace Cetria\Helpers\Reflection\Tests;

use function rand;
use function get_class;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use ReflectionException;

class getHiddenMethodTest extends TestCase
{
    public function testMethod__withInstance__privateMethod(): void
    {
        $instance = $this->getTestInstance();
        $method = Reflection::getHiddenMethod($instance, 'setValuePrivate');
        $newValue = rand(1, 50);
        $method->invoke($instance, $newValue);
        $this->assertEquals($newValue, $instance->value);
    }

    public function testMethod__withClass__privateMethod(): void
    {
        $instance = $this->getTestInstance();
        $method = Reflection::getHiddenMethod(get_class($instance), 'setValuePrivate');
        $newValue = rand(1, 50);
        $method->invoke($instance, $newValue);
        $this->assertEquals($newValue, $instance->value);
    }

    public function testMethod__withClass__privateStaticMethod(): void
    {
        $instance = $this->getTestInstance();
        $method = Reflection::getHiddenMethod(get_class($instance), 'setStaticValuePrivate');
        $newValue = rand(1, 50);
        $method->invoke($instance, $newValue);
        $this->assertEquals($newValue, $instance::$staticValue);
    }

    public function testMethod__nonexistentMethod(): void
    {
        $instance = $this->getTestInstance();
        $this->expectException(ReflectionException::class);
        Reflection::getHiddenMethod(\get_class($instance), 'nonexistentMethod');
    }


    private function getTestInstance(): object
    {
        return new class() 
        {
            public $value = 0;
            public static $staticValue = 0;

            private function setValuePrivate(int $value): void
            {
                $this->value = $value;
            }

            private static function setStaticValuePrivate(int $value): void
            {
                static::$staticValue = $value;
            }
        };
    }
}
