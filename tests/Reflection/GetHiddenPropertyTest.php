<?php

namespace Cetria\Helpers\Reflection\Tests;

use function rand;
use function get_class;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use ReflectionException;

class getHiddenPropertyTest extends TestCase
{
    public function testMethod__withInstance__privateProperty(): void
    {
        $value = rand(1, 50);
        $instance = $this->getTestInstance($value);
        $property = Reflection::getHiddenProperty($instance, 'privateProperty');
        $this->assertEquals($value, $property);
    }

    public function testMethod__withClass__privateStaticProperty(): void
    {
        $value = rand(1, 50);
        $instance = $this->getTestInstance(0, $value);
        $property = Reflection::getHiddenProperty(get_class($instance), 'privateStaticProperty');
        $this->assertEquals($value, $property);
    }

    public function testMethod__nonexistentProperty(): void
    {
        $instance = $this->getTestInstance();
        $this->expectException(ReflectionException::class);
        Reflection::getHiddenProperty(\get_class($instance), 'nonexistentProperty');
    }


    private function getTestInstance(int $privateProperty = 0, int $privateStaticProperty = 0): object
    {
        return new class($privateProperty, $privateStaticProperty) 
        {
            private static $privateStaticProperty;
            private $privateProperty;

            public function __construct(int $privateProperty, int $privateStaticProperty)
            {
                $this->privateProperty = $privateProperty;
                static::$privateStaticProperty = $privateStaticProperty;
            }
        };
    }
}
