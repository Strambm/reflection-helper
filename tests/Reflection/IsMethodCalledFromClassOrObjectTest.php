<?php

namespace Cetria\Helpers\Reflection\Tests;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;

class IsMethodCalledFromClassOrObjectTest extends TestCase
{
    public function testIsMethodCalledFromClassOrObjectReturnsTrueWhenMethodIsCalledFromObject(): void
    {
        $this->assertTrue(Reflection::isMethodCalledFromClassOrObject(self::class, __FUNCTION__));
    }

    public function testIsMethodCalledFromClassOrObjectReturnsTrueWhenMethodIsCalledFromClass(): void
    {
        $this->assertTrue(Reflection::isMethodCalledFromClassOrObject($this, __FUNCTION__));
    }

    public function testIsMethodCalledFromClassOrObjectReturnsFalseWhenMethodIsNotCalled(): void
    {
        $this->assertFalse(Reflection::isMethodCalledFromClassOrObject($this, 'nonExistentMethod'));
    }

    public function testIsMethodCalledFromClassOrObjectReturnsTrueWhenMethodIsCalledFromParentClass(): void
    {
        $this->assertTrue(Reflection::isMethodCalledFromClassOrObject($this, __FUNCTION__));
    }

    public function testIsMethodCalledFromClassOrObjectReturnsFalseWhenMethodIsNotCalledWithinBacktraceLimit(): void
    {
        $class = new class() {
            public function run(): bool
            {
                return Reflection::isMethodCalledFromClassOrObject(IsMethodCalledFromClassOrObjectTest::class, 'testIsMethodCalledFromClassOrObjectReturnsFalseWhenMethodIsNotCalledWithinBacktraceLimit', 1);
            }
        };
        $this->assertFalse($class->run());
    }

    public function testIsMethodCalledFromClassOrObjectReturnsTrueWhenMethodIsCalledWithinBacktraceLimit(): void
    {
        $class = new class() {
            public function run(): bool
            {
                return Reflection::isMethodCalledFromClassOrObject(IsMethodCalledFromClassOrObjectTest::class, 'testIsMethodCalledFromClassOrObjectReturnsTrueWhenMethodIsCalledWithinBacktraceLimit', 2);
            }
        };
        $this->assertTrue($class->run());
    }
}