<?php

namespace Cetria\Helpers\Reflection\Tests;

use Cetria\Helpers\Reflection\ClassDoesNotExistException;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HasUseTraitTest extends TestCase
{
    public function testMethod__withClass(): void
    {
        $result = Reflection::hasUseTrait(Product::class, HasFactory::class);
        $this->assertTrue($result);
    }

    public function testMethod__withInstance(): void
    {
        $result = Reflection::hasUseTrait(new Product(), HasFactory::class);
        $this->assertTrue($result);
    }

    public function testMethod__nonexistModelName(): void
    {
        $class = 'NonexistClass';
        $this->expectException(ClassDoesNotExistException::class);
        Reflection::hasUseTrait($class, HasFactory::class);
    }

    public function testMethod__returnFalse(): void
    {
        $unexistTrait = 'unexistTrait';
        $result = Reflection::hasUseTrait(Product::class, $unexistTrait);
        $this->assertFalse($result);
    }
}