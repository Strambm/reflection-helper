<?php

namespace Cetria\Helpers\Reflection\Tests;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Helpers\Reflection\Tests\Reflection\Dummy\TestEnum;

class IsEnumTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->skipIfPhpVersionIsTooLow();
    }

    public function testIsEnumReturnsTrueForEnum(): void
    {
        $this->assertTrue($this->act(TestEnum::CaseOne));
    }

    public function testIsEnumReturnsFalseForRegularClass(): void
    {
        $this->assertFalse($this->act($this));
    }

    private function act(object $classOrObject): bool
    {
        return Reflection::isEnum($classOrObject);
    }

    private function skipIfPhpVersionIsTooLow(): void
    {
        if (version_compare(PHP_VERSION, '8.1.0', '<')) {
            $this->markTestSkipped('The test applies only to PHP < 8.1.');
        }
    }
}