<?php

namespace Cetria\Helpers\Reflection\Tests;

use Cetria\Helpers\Reflection\ClassDoesNotExistException;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TraitUsesRecursiveTest extends TestCase
{
    public function testMethod__withClass(): void
    {
        $traits = Reflection::traitUsesRecursive(Product::class);
        $this->assertTrue(\in_array(HasFactory::class, $traits));
    }

    public function testMethod__withInstance(): void
    {
        $traits = Reflection::traitUsesRecursive(new Product());
        $this->assertTrue(\in_array(HasFactory::class, $traits));
    }

    public function testMethod__nonexistModelName(): void
    {
        $class = 'NonexistClass';
        $this->expectException(ClassDoesNotExistException::class);
        Reflection::traitUsesRecursive($class);
    }
}