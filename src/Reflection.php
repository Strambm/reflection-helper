<?php

namespace Cetria\Helpers\Reflection;

use ReflectionClass;
use ReflectionMethod;
use function get_class;
use function is_object;
use function class_uses;
use ReflectionException;
use function array_unique;
use function class_parents;

class Reflection
{
    /**
     * @param string|object $classOrObject
     * @return string[]
     * @throws ClassDoesNotExistException
     * @see \Cetria\Helpers\Reflection\Tests\ClassUsesRecursiveTest
     */
    public static function classUsesRecursive($classOrObject): array
    {
        $className = static::getClassName($classOrObject);
        static::throwIfClassDoesntExist($className);
        $results = [];
        foreach (class_parents($className) + [$className => $className] as $class) {
            $results += self::traitUsesRecursive($class);
        }
        return array_unique($results);
    }

    /**
     * @param string|object $classOrObject
     * @return ReflectionMethod
     * @throws ReflectionException
     */
    public static function getHiddenMethod($classOrObject, string $method): ReflectionMethod
    {
        $class = new ReflectionClass($classOrObject);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * @param string|object $classOrObject
     * @return mixed
     * @throws ReflectionException
     */
    public static function getHiddenProperty($classOrObject, string $property)
    {
        $reflection = new ReflectionClass($classOrObject);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        $objectOrNull = is_object($classOrObject) ? $classOrObject : null;
        return $property->getValue($objectOrNull);
    }

    /**
     * @param string|object $classOrObject
     * @param string $methodName
     * @param int $backtraceLimit
     * @return bool
     */
    public static function isMethodCalledFromClassOrObject($classOrObject, string $methodName, int $backtraceLimit = 0): bool
    {
        if($backtraceLimit > 0) {
            $backtraceLimit ++;
        }
        $className = self::getClassName($classOrObject);
        $traces = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, $backtraceLimit);
        /** @var array $trace*/
        foreach ($traces as $trace) {
            if (
                isset($trace['object']) 
                    && $trace['object'] instanceof $className
                    && isset($trace['function'])
                    && $trace['function'] == $methodName
            ) {
               return true;
            }
        }
        return false;
    }

    /**
     * @param string|object $class
     * @param string $property
     * @param mixed $value
     * @throws ReflectionException
     */
    public static function setHiddenProperty($class, string $property, $value)
    {
        $reflection = new ReflectionClass($class);
        $property = $reflection->getProperty($property);
        $property->setAccessible(true);
        $property->setValue($class, $value);
    }

    /**
     * @param string|object $class
     * @return string[]
     * @throws ClassDoesNotExistException
     */
    public static function traitUsesRecursive($class): array
    {
        $className = static::getClassName($class);
        self::throwIfClassDoesntExist($className);
        $traits = class_uses($className) ?: [];
        foreach ($traits as $trait) {
            $traits += self::traitUsesRecursive($trait);
        }
        return $traits;
    }

    /**
     * @param string|object $classOrObject
     * @throws ClassDoesNotExistException
     * @see \Cetria\Helpers\Reflection\Tests\HasUseTraitTest
     */
    public static function hasUseTrait($classOrObject, string $traitName): bool
    {
        $traitUses = self::classUsesRecursive($classOrObject);
        return in_array($traitName, $traitUses);
    }

    /**
     * @see \Cetria\Helpers\Reflection\Tests\IsEnumTest
     */
    public static function isEnum(object $classOrObject): bool
    {
        if (
            version_compare(PHP_VERSION, '8.1.0', '>=')
        ) {
            return (new ReflectionClass($classOrObject))->isEnum();
        } else {
            return false;
        }
    }

    /**
     * @param string|object $classOrObject
     */
    private static function getClassName($classOrObject): string
    {
        if(is_object($classOrObject)) {
            return get_class($classOrObject);
        } else {
            return (string) $classOrObject;
        }
    }

    /**
     * @throws ClassDoesNotExistException
     */
    private static function throwIfClassDoesntExist(string $className): void
    {
        if(
            !class_exists($className)
                && !trait_exists($className)
        ) {
            throw new ClassDoesNotExistException($className);
        }   
    } 
}
