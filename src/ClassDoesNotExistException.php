<?php

namespace Cetria\Helpers\Reflection;

use Exception;

class ClassDoesNotExistException extends Exception
{
    public function __construct(string $class, int $code = 0, $previous = null)
    {
        $msg = $this->msg($class);
        parent::__construct($msg, $code, $previous);
    }

    private function msg(string $class): string
    {
        return 'Class \'' . $class . '\' Does not exist!';
    }
}
